import {
  Body,
  SetMetadata,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  HttpCode,
  HttpStatus,
  Query
} from "@nestjs/common";
import { CoffeesService } from "./coffees.service";
import { CreateCoffeeDto } from "./dto/create-coffee.dto";
import { UpdateCoffeeDto } from "./dto/update-coffee.dto";
import { PaginationQueryDto } from "./dto/pagination-query.dto";
import { Public } from "../common/decorators/public.decorators";
import { ParseIntPipe } from "../common/pipes/parse-int/parse-int.pipe";
import { Protocol } from "../common/decorators/protocol.decorators";
import { ApiForbiddenResponse, ApiTags } from "@nestjs/swagger";

@ApiTags('coffee')
@Controller("coffees")
export class CoffeesController {
  constructor(private readonly coffeesService: CoffeesService) {
  }

  @ApiForbiddenResponse({description: 'Forbidden'})
  @Public()
  @Get()
  async findAll(@Protocol('https') protocol: string ,@Query() paginationQuery: PaginationQueryDto) {
    // await new Promise(resolve => setTimeout(resolve,5000))
    console.log(protocol);
    return this.coffeesService.findAll(paginationQuery);
  }

  @Public()
  @Get(":id")
  findOne(@Param("id",ParseIntPipe) id: number) {
    console.log(id);
    return this.coffeesService.findOne(id);
  }

  @Public()
  @Post()
  @HttpCode(HttpStatus.GONE)
  create(@Body() createCoffeeDto: CreateCoffeeDto) {
    return this.coffeesService.create(createCoffeeDto);
  }

  @Public()
  @Patch(":id")
  update(@Param("id") id: string, @Body() updateCoffeeDtoTs: UpdateCoffeeDto) {
    return this.coffeesService.update(id, updateCoffeeDtoTs);

  }

  @Public()
  @Delete(":id")
  remove(@Param("id") id: string) {
    return this.coffeesService.remove(id);
  }
}
