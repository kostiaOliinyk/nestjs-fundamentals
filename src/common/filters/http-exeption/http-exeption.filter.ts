import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from "@nestjs/common";
import express, {Request, Response} from 'express';

@Catch(HttpException)
export class HttpExeptionFilter<T extends HttpException>
  implements ExceptionFilter {
  catch(exception: T, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    const status = exception.getStatus();
    const exceptionResponse = exception.getResponse();
    const error =
      typeof response === 'string'
        ? { message: exceptionResponse}
        : (exceptionResponse as object);

    response.status(200).json({
      ...error,
      timestamp: new Date().toISOString(),
    })

  }
}
