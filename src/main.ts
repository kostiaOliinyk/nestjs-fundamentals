import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { ValidationPipe } from "@nestjs/common";
import { HttpExeptionFilter } from "./common/filters/http-exeption/http-exeption.filter";
import { ApiKeyGuard } from "./common/guards/api-key/api-key.guard";
import { WrapResponseInterceptor } from "./common/interceptors/wrap-response/wrap-response.interceptor";
import { TimeoutInterceptor } from "./common/interceptors/timeout/timeout.interceptor";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
      transformOptions: {
        enableImplicitConversion: true
      }
    }
  ));
  const options = new DocumentBuilder()
    .setTitle('Iluvcoffe')
    .setDescription('Coffee application')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api',app,document);
  app.useGlobalFilters(new HttpExeptionFilter());
  app.useGlobalInterceptors(new WrapResponseInterceptor(), new TimeoutInterceptor())
  await app.listen(3000);
}

bootstrap();
